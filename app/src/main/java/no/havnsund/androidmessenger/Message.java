package no.havnsund.androidmessenger;
import android.app.Activity;
import android.content.ContentUris;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.ContactsContract;
import android.widget.ImageView;
import java.io.IOException;
import java.io.InputStream;
import java.util.Random;

import static no.havnsund.androidmessenger.Message.Category.PERSONAL;


/**
 * Created by Humanic on 02/09/16.
 */

public class Message extends Activity {
    private String author, message;
    private long dateCreatedMilli;
    private int noteId;
    private Category category;

    public enum Category{PERSONAL, TECHNICAL, QUOTE, FINANCE}

    public Message(String author, String message){
        this.author = author;
        this.message = message;

    }
    public Message(String author, String message, Category category){
        this.author = author;
        this.message = message;
        this.category = category;
        this.noteId = 0;
        this.dateCreatedMilli = 0;

    }
    public Message(String author, String message, Category category, int noteId){
        this.author = author;
        this.message = message;
        this.category = category;
        this.noteId = noteId;
    }

    public Message(String author, String message, Category category, int noteId, long dateCreatedMilli){
        this.author = author;
        this.message = message;
        this.category = category;
        this.noteId = noteId;
        this.dateCreatedMilli = dateCreatedMilli;
    }



    //TODO Implement a way to get pics from the contact.
/*
    public int getPic(){
        Uri uri = Uri.withAppendedPath(ContactsContract.Contacts.CONTENT_FILTER_URI,Uri.encode(author.trim()));
        Cursor mapContact = this.getContentResolver().query(uri, new String[]{ContactsContract.PhoneLookup._ID}, null, null, null);
        int id = 0;
        if(mapContact.moveToNext())
        {
            id = mapContact.getInt(mapContact.getColumnIndex(ContactsContract.Contacts._ID));

        }
        else{
            return id;
        }
        return id;
    }
*/
    /*
    public static Bitmap retrieveContactPhoto(Context context, String number) {
        ContentResolver contentResolver = context.getContentResolver();
        String contactId = null;
        Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(number));

        String[] projection = new String[]{ContactsContract.PhoneLookup.DISPLAY_NAME, ContactsContract.PhoneLookup._ID};

        Cursor cursor =
                contentResolver.query(
                        uri,
                        projection,
                        null,
                        null,
                        null);

        if (cursor != null) {
            while (cursor.moveToNext()) {
                contactId = cursor.getString(cursor.getColumnIndexOrThrow(ContactsContract.PhoneLookup._ID));
            }
            cursor.close();
        }

        Bitmap photo = BitmapFactory.decodeResource(context.getResources(),
                R.drawable.a);

        try {
            InputStream inputStream = ContactsContract.Contacts.openContactPhotoInputStream(context.getContentResolver(),
                    ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, new Long(contactId)));

            if (inputStream != null) {
                photo = BitmapFactory.decodeStream(inputStream);
            }

            assert inputStream != null;
            inputStream.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
        return photo;
    }
    */

    public Uri getPhotoUri() {
        try {
            Cursor cur = this.getContentResolver().query(
                    ContactsContract.Data.CONTENT_URI,
                    null,
                    ContactsContract.Data.CONTACT_ID + "=" + author + " AND "
                            + ContactsContract.Data.MIMETYPE + "='"
                            + ContactsContract.CommonDataKinds.Photo.CONTENT_ITEM_TYPE + "'", null,
                    null);
            if (cur != null) {
                if (!cur.moveToFirst()) {
                    return null; // no photo
                }
            } else {
                return null; // error in cursor process
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        Uri person = ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, Long
                .parseLong(author));
        return Uri.withAppendedPath(person, ContactsContract.Contacts.Photo.CONTENT_DIRECTORY);
    }

    public void retrieveContactPhoto(int contactID) {

        Bitmap photo = null;

        try {
            InputStream inputStream = ContactsContract.Contacts.openContactPhotoInputStream(getContentResolver(),
                    ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, new Long(contactID)));

            if (inputStream != null) {
                photo = BitmapFactory.decodeStream(inputStream);
                ImageView imageView = (ImageView) findViewById(R.id.listItemMessageImg);
                if(photo != null) {
                    imageView.setImageBitmap(photo);
                }else{
                    imageView.setImageResource(getAssociatedDrawable());
                }
            }

            assert inputStream != null;
            inputStream.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }
/*
    public Uri getPhotoUri(int contactid) {
        Cursor photoCur = getContentResolver().query(ContactsContract.Contacts.CONTENT_URI,null, ContactsContract.Contacts.IN_VISIBLE_GROUP + " = '1'", null, ContactsContract.Contacts.DISPLAY_NAME+" COLLATE LOCALIZED ASC");
        photoCur.moveToPosition(contactid);
        Uri person = ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, photoCur.getLong(photoCur.getColumnIndex(ContactsContract.Contacts._ID)));
        Uri photo = Uri.withAppendedPath(person, ContactsContract.Contacts.Photo.CONTENT_DIRECTORY);
        return photo;
    }
*/
    /*
public static int getContactIDFromNumber(String contactNumber,Context context)
{
    contactNumber = Uri.encode(contactNumber);
    int phoneContactID = new Random().nextInt();
    Cursor contactLookupCursor = context.getContentResolver().query(Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI,contactNumber),new String[] {ContactsContract.PhoneLookup.DISPLAY_NAME, PhoneLookup._ID}, null, null, null);
    while(contactLookupCursor.moveToNext()){
        phoneContactID = contactLookupCursor.getInt(contactLookupCursor.getColumnIndexOrThrow(PhoneLookup._ID));
    }
    contactLookupCursor.close();

    return phoneContactID;
}
    /*
    public void GetContactPhoto(int contactID) {

        Bitmap photo = null;

        try {
            InputStream inputStream = ContactsContract.Contacts.openContactPhotoInputStream(getContentResolver(),
                    ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, new Long(contactID)));

            if (inputStream != null) {
                photo = BitmapFactory.decodeStream(inputStream);
                ImageView imageView = (ImageView) findViewById(R.id.listItemMessageImg);
                imageView.setImageBitmap(photo);
            }

            assert inputStream != null;
            inputStream.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }
*/


    public String getAuthor() {
        return author;
    }

    public String getMessage() {
        return message;
    }

    public int getNoteId() {
        return noteId;
    }

    public Category getCategory() {
        return category;
    }

    public long getDate() {
        return dateCreatedMilli;
    }

    public String toString(){
        return "ID: " + noteId + " Author: " + author + " Message: " + message + " IconID: " + category.name()
                + " Date: ";
    }

    public int getAssociatedDrawable(){return categoryToDrawable(category);}

    public static int drawable(){
        return R.drawable.a;
    }

    public static int categoryToDrawable(Category messageCategory){
        switch(messageCategory) {
            case PERSONAL:
                return R.drawable.a;
            case TECHNICAL:
                return R.drawable.b;
            case FINANCE:
                return R.drawable.c;
            case QUOTE:
                return R.drawable.s;
        }
        return R.drawable.a;
    }
}
