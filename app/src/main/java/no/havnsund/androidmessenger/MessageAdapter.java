package no.havnsund.androidmessenger;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.ContactsContract;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.util.ArrayList;

/**
 * Created by Humanic on 02/09/16.
 */

public class MessageAdapter extends ArrayAdapter<Message> {

    public MessageAdapter(Context context, ArrayList<Message> messages){
        super(context, 0, messages);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        //Get the data item for this position
        Message message = getItem(position);

        //Check if an existing view is being reused, otherwise inflate a new view form custom row layout.
        if(convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.messenger_main, parent, false);
            //convertView = LayoutInflater.from(getContext()).inflate(R.layout.content_main, parent, false);

        }

        TextView messageAuthor = (TextView) convertView.findViewById(R.id.listItemMessageAuthor);
        TextView messageText = (TextView) convertView.findViewById(R.id.listItemMessageBody);
        ImageView messageIcon = (ImageView) convertView.findViewById(R.id.listItemMessageImg);

        messageAuthor.setText(message.getAuthor());
        messageText.setText(message.getMessage());
        messageIcon.setImageResource(message.getAssociatedDrawable());
        //message.retrieveContactPhoto(message.getNoteId());
        /*
        int contactID = message.getContactIDFromNumber("12345678", getContext());
        Uri u = message.getPhotoUri(contactID);
        if(u != null){
            messageIcon.setImageURI(u);
        }else{
            messageIcon.setImageResource(message.getAssociatedDrawable());
        }
        */
        //messageIcon.setImageResource(message.getAssociatedDrawable());
        /*
        Uri u = message.getPhotoUri();
        messageIcon.setImageURI(u);
        */
        /*
        Uri u = message.getPhotoUri();

        if(u != null){
            messageIcon.setImageURI(u);
        }else {

            //messageIcon.setImageResource(message.drawable());
            messageIcon.setImageResource(message.getAssociatedDrawable());
        }
        */

        /*
        Bitmap bm = message.retrieveContactPhoto(getContext(), "12345678");
        messageIcon.setImageBitmap(bm);
        */
        /*
        int id = message.getPic();
        if(id != 0){
            messageIcon.setImageResource(id);
        }else{
            messageIcon.setImageResource(message.drawable());
        }
        */
         //messageIcon.setImageResource(id);
        /*
        Uri u = message.getPhotoUri();
        if(u != null){
            messageIcon.setImageURI(u);
        }else{
            messageIcon.setImageResource(message.getAssociatedDrawable());
        }
        */
        /*
        Uri my_contact_Uri = Uri.withAppendedPath(ContactsContract.Contacts.CONTENT_URI, String.valueOf(2));
        InputStream photo_stream = ContactsContract.Contacts.openContactPhotoInputStream(message.getContentResolver(), my_contact_Uri);
        BufferedInputStream buf = new BufferedInputStream(photo_stream);
        Bitmap my_btmp = BitmapFactory.decodeStream(buf);
        messageIcon.setImageBitmap(my_btmp);
        */

        // now that we modified the view to display appropriatedata,
        // return it so it will be displayed.

        return convertView;
    }

}
