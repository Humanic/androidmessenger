package no.havnsund.androidmessenger;


import android.content.Intent;
import android.os.Bundle;
//import android.app.Fragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 */
public class MessageViewFragment extends Fragment {


    public MessageViewFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View fragmentLayout = inflater.inflate(R.layout.fragment_message_view, container, false);

        TextView title = (TextView) fragmentLayout.findViewById(R.id.viewMessageTitle);
        TextView message = (TextView) fragmentLayout.findViewById(R.id.viewMessageMessage);
        ImageView icon = (ImageView) fragmentLayout.findViewById(R.id.viewMessageIcon);

        Intent intent = getActivity().getIntent();

        title.setText(intent.getExtras().getString(MainActivity.MESSAGE_TITLE_EXTRA));
        message.setText(intent.getExtras().getString(MainActivity.MESSAGE_MESSAGE_EXTRA));

        Message.Category messageCat = (Message.Category) intent.getSerializableExtra(MainActivity.MESSAGE_CATEGORY_EXTRA);
        icon.setImageResource(Message.categoryToDrawable(messageCat));

        ImageButton send = (ImageButton) fragmentLayout.findViewById(R.id.messageSendButton);
        final EditText textInput = (EditText) fragmentLayout.findViewById(R.id.messageInput);
        final TextView textView = (TextView) fragmentLayout.findViewById(R.id.viewMessageMessage);
        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                textView.setText(textInput.getText());
                textInput.setText(null);
            }
        });
        // Inflate the layout for this fragment
        return fragmentLayout;
    }

}
