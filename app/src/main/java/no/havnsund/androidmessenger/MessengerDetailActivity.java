package no.havnsund.androidmessenger;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MessengerDetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_messenger_detail);

        createAndAddFragment();
    }

    private void createAndAddFragment(){

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        MessageViewFragment messageViewFragment = new MessageViewFragment();
        setTitle(R.string.viewFragmentTitle);
        fragmentTransaction.add(R.id.messenger_container, messageViewFragment, "MESSAGE_VIEW_FRAGMENT");

        fragmentTransaction.commit();
    }
}
