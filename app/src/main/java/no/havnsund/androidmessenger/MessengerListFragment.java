package no.havnsund.androidmessenger;

import android.Manifest;
import android.app.Activity;
import android.app.ListActivity;
import android.content.ContentUris;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Random;

import static no.havnsund.androidmessenger.Message.Category.FINANCE;
import static no.havnsund.androidmessenger.Message.Category.PERSONAL;
import static no.havnsund.androidmessenger.Message.Category.QUOTE;
import static no.havnsund.androidmessenger.Message.Category.TECHNICAL;

/**
 * Created by Humanic on 02/09/16.
 */

public class MessengerListFragment extends ListActivity {

    private ArrayList<Message> messages;
    private MessageAdapter messageAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.READ_CONTACTS},1);

        messages = new ArrayList<>();

        /*
        Message m = new Message("Geir", "Hello world, what are you doiiing?", Message.Category.TECHNICAL);

        messages.add(m);
        messages.add(new Message("Mikael", "Hello world, what are you up to?",
                Message.Category.PERSONAL));
        messages.add(new Message("Jørgen", "Hello world, what are you up to?",
                Message.Category.TECHNICAL));
        messages.add(new Message("Tor", "Hello world, what are you up to?",
                Message.Category.FINANCE));
        messages.add(new Message("Andreas", "Hello world, what are you up to?",
                Message.Category.QUOTE));
        messages.add(new Message("Bjørnar", "Hello world, what are you up to?",
                Message.Category.PERSONAL));
        messages.add(new Message("Trond", "Hello world, what are you up to?",
                Message.Category.PERSONAL));
        messages.add(new Message("Petter", "Hello world, what are you up to?",
                Message.Category.TECHNICAL));
        messages.add(new Message("Lars", "Hello world, what are you up to?",
                Message.Category.FINANCE));
        messages.add(new Message("Leif", "Hello world, what are you up to?",
                Message.Category.FINANCE));
        messages.add(new Message("Espen", "Hello world, what are you up to?",
                Message.Category.QUOTE));
        messages.add(new Message("Eirik", "Hello world, what are you up to?",
                Message.Category.PERSONAL));
        messages.add(new Message("Inge", "Hello world, what are you up to?",
                Message.Category.QUOTE));
*/


        //Loops through all the contacts and makes a new message dialog for each contact found with an phone number.
        Cursor cursor = getContentResolver().query(ContactsContract.Contacts.CONTENT_URI,null, null, null, null);
        while (cursor.moveToNext()) {
            String contactId = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
            String hasPhone = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER));
            String name = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
            //Uri photo = getPhotoUri(Integer.parseInt(contactId));

            if(contactId != null || name != null) {
                messages.add(new Message(name, "hei", this.getRandomCategory(), Integer.parseInt(contactId)));
            }else{
                messages.add(new Message("Error", "couldn't load this contact", this.getRandomCategory(), 123));
            }

            /*
            if (Boolean.parseBoolean(hasPhone)) {
                // You know it has a number so now query it like this
                Cursor phones = getContentResolver().query( ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID +" = "+ contactId, null, null);
                while (phones.moveToNext()) {
                    //String phoneNumber = phones.getString(phones.getColumnIndex( ContactsContract.CommonDataKinds.Phone.NUMBER));
                    messages.add(new Message(name, "", Message.Category.QUOTE, Integer.parseInt(contactId)));
                }
                phones.close();
            }
            */
        }
        cursor.close();



        messageAdapter = new MessageAdapter(getApplicationContext(), messages);
        setListAdapter(messageAdapter);

        //If the grey line doesn't split the messages, use this piece of code. And if you want to change
        //the color or the thickness of the line.
        getListView().setDivider(ContextCompat.getDrawable(this, android.R.color.holo_blue_dark));
        getListView().setDividerHeight(2);


    }

    public Message.Category getRandomCategory(){
        Random r = new Random();
        int i = r.nextInt(4);
        Message.Category randCat = PERSONAL;
        switch(i){
            case 0:
                randCat = QUOTE;
                return randCat;

            case 1:
                randCat = TECHNICAL;
                return randCat;

            case 2:
                randCat = FINANCE;
                return randCat;

            case 3:
                randCat = PERSONAL;
                return randCat;
        }
        return randCat;
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id){
        super.onListItemClick(l, v, position, id);
        //TODO implement single chats for each contact, where they have a text input, send button,
        //TODO text feed, names and pics are retrieved from the contacts on the phone.
        launchMessengerDetailActivity(position);


    }
    private void launchMessengerDetailActivity(int position){

        //Grab the message information associated with the whatever message item we clicked on
        Message message = (Message) getListAdapter().getItem(position);

        //Create a new intent that launches our MessengerDetailActivity
        Intent intent = new Intent(this, MessengerDetailActivity.class);

        //Pass along the information of the message we clicked on to our messengerDetailActivity
        intent.putExtra(MainActivity.MESSAGE_TITLE_EXTRA, message.getAuthor());
        intent.putExtra(MainActivity.MESSAGE_MESSAGE_EXTRA, message.getMessage());
        intent.putExtra(MainActivity.MESSAGE_CATEGORY_EXTRA, message.getCategory());
        intent.putExtra(MainActivity.MESSAGE_ID_EXTRA, message.getNoteId());

        startActivity(intent);
    }



}
